package gathercraft.service.result.item;

import gathercraft.db.model.readonly.ItemDataModel;
import gathercraft.db.model.readonly.ItemModel;
import gathercraft.util.Language;

public class LoadItemResult {
    public String name;
    public String description;
    public int difficulty;
    public int value;

    public LoadItemResult(final ItemModel item, final Language language) {
        final ItemDataModel data = item.getLocalizedData().get(language);
        name = data.getName();
        description = data.getDescription();
        difficulty = item.getDifficulty();
        value = item.getValue();
    }
}
