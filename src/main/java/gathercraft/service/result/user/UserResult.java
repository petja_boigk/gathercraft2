package gathercraft.service.result.user;

import gathercraft.db.model.UserModel;

public class UserResult {
    public String token;

    public UserResult(final UserModel user) {
        this.token = user.getToken();
    }
}
