package gathercraft.service;

import javax.transaction.Transactional;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

import gathercraft.db.dao.ItemDao;
import gathercraft.db.model.readonly.ItemModel;
import gathercraft.service.request.item.LoadItemRequest;
import gathercraft.service.result.item.LoadItemResult;
import gathercraft.util.Language;

@Component
@Path("/item/")
@Transactional
public class ItemService extends BaseService {

    @Autowired
    private ItemDao itemDao;

    @POST
    @Path("/")
    @Consumes(value = MediaType.APPLICATION_JSON_VALUE)
    @Produces(value = MediaType.APPLICATION_JSON_VALUE)
    public LoadItemResult loadItem(final LoadItemRequest request) {
        if (request == null) {
            throw new BadRequestException();
        }
        final ItemModel model = itemDao.loadItemById(request.id);

        if (model == null) {
            throw new BadRequestException();
        }

        return new LoadItemResult(model, Language.mapTo(request.language));
    }
}
