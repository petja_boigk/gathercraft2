package gathercraft.config;

import java.util.Properties;

import javax.sql.DataSource;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.AvailableSettings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.hibernate4.HibernateTransactionManager;
import org.springframework.orm.hibernate4.LocalSessionFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
@ComponentScan(basePackages = "gathercraft")
@PropertySource(value = {"classpath:application.properties" })
public class AppConfig {

    @Autowired
    private Environment env;

    @Bean
    public LocalSessionFactoryBean sessionFactory() {
        final LocalSessionFactoryBean sessionFactory = new LocalSessionFactoryBean();
        sessionFactory.setDataSource(restDataSource());
        sessionFactory.setPackagesToScan(new String[] {"gathercraft.db.model" });
        sessionFactory.setHibernateProperties(hibernateProperties());

        return sessionFactory;
    }

    @Bean
    public DataSource restDataSource() {
        final DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(env.getRequiredProperty("datasource.driver"));
        dataSource.setUrl(env.getRequiredProperty("datasource.url"));
        dataSource.setUsername(env.getRequiredProperty("datasource.username"));
        dataSource.setPassword(env.getRequiredProperty("datasource.password"));
        return dataSource;
    }

    @Bean
    @Autowired
    public PlatformTransactionManager transactionManager(final SessionFactory sessionFactory) {
        final HibernateTransactionManager txManager = new HibernateTransactionManager();
        txManager.setSessionFactory(sessionFactory);
        DBInit.init(sessionFactory);
        return txManager;
    }

    @Bean
    public PersistenceExceptionTranslationPostProcessor exceptionTranslation() {
        return new PersistenceExceptionTranslationPostProcessor();
    }

    private Properties hibernateProperties() {
        final Properties properties = new Properties();
        properties.put(AvailableSettings.DIALECT, env.getRequiredProperty("hibernate.dialect"));
        properties.put(AvailableSettings.SHOW_SQL, env.getRequiredProperty("hibernate.show_sql"));

        properties.put(AvailableSettings.STATEMENT_BATCH_SIZE,
                env.getRequiredProperty("hibernate.batch.size"));
        properties.put(AvailableSettings.HBM2DDL_AUTO,
                env.getRequiredProperty("hibernate.hbm2ddl.auto"));
        properties.put(AvailableSettings.FORMAT_SQL, true);

        properties.put(AvailableSettings.AUTOCOMMIT,
                env.getRequiredProperty("hibernate.connection.autocommit"));
        properties.put(AvailableSettings.CURRENT_SESSION_CONTEXT_CLASS,
                env.getRequiredProperty("hibernate.current.session.context.class"));
        properties.put(AvailableSettings.CACHE_REGION_FACTORY,
                env.getRequiredProperty("hibernate.cache.region.factory_class"));
        properties.put(AvailableSettings.USE_SECOND_LEVEL_CACHE,
                env.getRequiredProperty("hibernate.cache.use_second_level_cache"));
        properties.put(AvailableSettings.USE_QUERY_CACHE,
                env.getRequiredProperty("hibernate.cache.use_query_cache"));
        return properties;
    }

}
