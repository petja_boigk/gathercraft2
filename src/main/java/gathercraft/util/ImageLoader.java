package gathercraft.util;

import java.io.File;
import java.io.FileInputStream;
import java.util.Base64;

import org.apache.commons.io.IOUtils;
import org.springframework.stereotype.Component;

@Component
public class ImageLoader {

	public String loadAndEncodeImage(String path) {
		try {
			ClassLoader classLoader = getClass().getClassLoader();
			File file = new File(classLoader.getResource(path).getFile());
			if (file.exists()) {
				FileInputStream stream = new FileInputStream(file);
				byte[] imageBytes = IOUtils.toByteArray(stream);
				String base64 = Base64.getEncoder().encodeToString(imageBytes);
				return base64;
			}
		} catch(Exception e) {
			System.err.println(e.getMessage());
		}
		return null;
	}
	
}
