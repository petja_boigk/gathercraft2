package gathercraft.db.dao;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class BaseDao {

    @Autowired
    protected SessionFactory sessionFactory;

    protected Object loadById(final long id, final Class<?> clazz) {
        final Session session = sessionFactory.getCurrentSession();
        try {
            final Object model = session.get(clazz, id);
            return model;
        } catch (final HibernateException ex) {
            return null;
        }
    }
}
