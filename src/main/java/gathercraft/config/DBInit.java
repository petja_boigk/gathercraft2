package gathercraft.config;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import gathercraft.db.model.readonly.ItemDataModel;
import gathercraft.db.model.readonly.ItemModel;
import gathercraft.util.Language;

public class DBInit {

    private static Map<Long, ItemModel> itemMap = new HashMap<>();

    public static void init(final SessionFactory sessionFactory) {
        final Session session = sessionFactory.openSession();
        final Transaction tx = session.beginTransaction();
        addItems(session);

        tx.commit();
        session.flush();
        session.close();
    }

    private static void addItems(final Session session) {
        final List<ItemModel> models = new ArrayList<>();

        final ItemModel log = new ItemModel(100, 50);
        final ItemDataModel deLog =
                new ItemDataModel(log, Language.DE, "Stamm", "Baumstamm");
        final ItemDataModel enLog =
                new ItemDataModel(log, Language.EN, "Log", "Log of tree");
        initItem(log, new ItemDataModel[] {deLog, enLog }, models);

        final ItemModel ore = new ItemModel(100, 50);
        final ItemDataModel deOre = new ItemDataModel(ore, Language.DE, "Erz", "Erz");
        final ItemDataModel enOre = new ItemDataModel(ore, Language.EN, "Ore", "Ore");
        initItem(ore, new ItemDataModel[] {deOre, enOre }, models);

        final ItemModel skin = new ItemModel(100, 50);
        final ItemDataModel deSkin =
                new ItemDataModel(skin, Language.DE, "Tierhaut", "Tierhaut");
        final ItemDataModel enSkin =
                new ItemDataModel(skin, Language.EN, "AnimalSkin", "AnimalSkin");
        initItem(skin, new ItemDataModel[] {deSkin, enSkin }, models);

        for (final ItemModel model : models) {
            itemMap.put(model.getId(), model);
            session.save(model);
        }
    }

    private static void initItem(final ItemModel model, final ItemDataModel[] localizedModels,
            final List<ItemModel> modelList) {
        final Map<Language, ItemDataModel> map = new HashMap<>();
        for (final ItemDataModel lModel : localizedModels) {
            map.put(lModel.getLanguage(), lModel);
        }

        model.setLocalizedData(map);
        modelList.add(model);
    }

    private static void addTables(final Session session) {

    }

}
