package gathercraft.db.dao;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Component;

import gathercraft.db.model.UserModel;

@Component
public class UserDao extends BaseDao {
    public enum CreateUserResult {
        OK, LOGINEXIST, ERROR
    }

    public enum UpdateUserResult {
        OK, LOGINNOTEXIST, ERROR
    }

    public CreateUserResult createUser(final UserModel user) {
        final UserModel model = loadUserByLogin(user.getLogin());
        if (model != null) {
            return CreateUserResult.LOGINEXIST;
        }

        final Session session = sessionFactory.getCurrentSession();

        try {
            session.save(user);
            return CreateUserResult.OK;
        } catch (final HibernateException ex) {
            return CreateUserResult.ERROR;
        }
    }

    public UpdateUserResult updateUser(final UserModel user) {
        final UserModel model = loadUserByLogin(user.getLogin());
        if (model == null) {
            return UpdateUserResult.LOGINNOTEXIST;
        }

        final Session session = sessionFactory.getCurrentSession();

        try {
            model.setName(user.getName());
            model.setToken(user.getToken());

            session.update(user);
            return UpdateUserResult.OK;

        } catch (final HibernateException ex) {
            return UpdateUserResult.ERROR;
        }
    }

    public UserModel loadUserById(final long id) {
        final Session session = sessionFactory.getCurrentSession();
        try {
            final UserModel model = (UserModel) session.get(UserModel.class, id);
            return model;
        } catch (final HibernateException ex) {
            return null;
        }
    }

    public UserModel loadUserByLogin(final String login) {
        final Session session = sessionFactory.getCurrentSession();
        try {
            final UserModel model = (UserModel) session.createCriteria(UserModel.class)
                    .add(Restrictions.eq("login", login)).uniqueResult();
            return model;
        } catch (final HibernateException ex) {
            return null;
        }
    }

}
