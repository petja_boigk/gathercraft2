package gathercraft.util;

public enum Language {
    DE, EN;

    public static Language mapTo(final String value) {
        return Language.valueOf(value.toUpperCase());
    }
}
