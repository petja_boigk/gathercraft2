package gathercraft.db.model.readonly;

import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

@Entity
@Cacheable
@Cache(usage=CacheConcurrencyStrategy.READ_ONLY)
public class GatheringPointModel {

    private long id;
    private ItemModel item;
    private int rarity;

    public GatheringPointModel() {
    }

    public GatheringPointModel(final ItemModel item, final int rarity) {
        this.item = item;
        this.rarity = rarity;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public long getId() {
        return id;
    }

    public void setId(final long id) {
        this.id = id;
    }

    @ManyToOne(cascade = CascadeType.ALL)
    public ItemModel getItem() {
        return item;
    }

    public void setItem(final ItemModel item) {
        this.item = item;
    }

    public int getRarity() {
        return rarity;
    }

    public void setRarity(final int rarity) {
        this.rarity = rarity;
    }
}
