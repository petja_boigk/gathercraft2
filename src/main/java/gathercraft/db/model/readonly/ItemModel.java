package gathercraft.db.model.readonly;

import java.util.Map;

import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MapKeyEnumerated;
import javax.persistence.OneToMany;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import gathercraft.util.Language;

@Entity
@Cacheable
@Cache(usage=CacheConcurrencyStrategy.READ_ONLY)
public class ItemModel {
    private long id;

    private Map<Language, ItemDataModel> localizedData;

    private int difficulty;
    private int value;

    public ItemModel() {
    }

    public ItemModel(final int difficulty, final int value) {
        this.difficulty = difficulty;
        this.value = value;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public long getId() {
        return id;
    }

    public void setId(final long id) {
        this.id = id;
    }

    @Cache(usage=CacheConcurrencyStrategy.READ_ONLY)
    @OneToMany(mappedBy = "item", cascade = CascadeType.ALL)
    @MapKeyEnumerated(EnumType.STRING)
    public Map<Language, ItemDataModel> getLocalizedData() {
        return localizedData;
    }

    public void setLocalizedData(final Map<Language, ItemDataModel> localizedData) {
        this.localizedData = localizedData;
    }

    public int getDifficulty() {
        return difficulty;
    }

    public void setDifficulty(final int difficulty) {
        this.difficulty = difficulty;
    }

    public int getValue() {
        return value;
    }

    public void setValue(final int value) {
        this.value = value;
    }
}
