package gathercraft.service.request.user;

import gathercraft.service.request.BaseRequest;

public class UserRequest extends BaseRequest {
    public String name;
    public String login;
    public String password;
}
