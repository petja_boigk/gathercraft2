package gathercraft.db.model.readonly;

import java.util.Map;
import java.util.Set;

import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MapKeyEnumerated;
import javax.persistence.OneToMany;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import gathercraft.util.Language;

@Entity
@Cacheable
@Cache(usage=CacheConcurrencyStrategy.READ_ONLY)
public class AreaModel {

	private long id;
	private Map<Language, AreaDataModel> localizedData;
	private Set<GatheringSpotModel> spots;
	
	public AreaModel(Map<Language, AreaDataModel> localizedData, Set<GatheringSpotModel> spots) {
		this.localizedData = localizedData;
		this.spots = spots;
	}
	
	public AreaModel() {
	}
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	
	@Cache(usage=CacheConcurrencyStrategy.READ_ONLY)
    @OneToMany(mappedBy = "area", cascade = CascadeType.ALL)
    @MapKeyEnumerated(EnumType.STRING)
	public Map<Language, AreaDataModel> getLocalizedData() {
		return localizedData;
	}
	
	public void setLocalizedData(Map<Language, AreaDataModel> localizedData) {
		this.localizedData = localizedData;
	}

	@Cache(usage=CacheConcurrencyStrategy.READ_ONLY)
	@OneToMany(cascade = CascadeType.ALL)
	public Set<GatheringSpotModel> getSpots() {
		return spots;
	}

	public void setSpots(Set<GatheringSpotModel> spots) {
		this.spots = spots;
	}
	
	
	
}
