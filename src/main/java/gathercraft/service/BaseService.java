package gathercraft.service;

import javax.ws.rs.BadRequestException;
import javax.ws.rs.NotAuthorizedException;

import org.springframework.beans.factory.annotation.Autowired;

import gathercraft.db.dao.UserDao;
import gathercraft.db.model.UserModel;
import gathercraft.service.request.BaseRequest;

public class BaseService {

    @Autowired
    protected UserDao userDao;

    public void checkLogin(final BaseRequest request) {
        if (request == null) {
            throw new BadRequestException();
        }
        final UserModel model = userDao.loadUserByLogin(request.login);
        if (!model.getToken().equals(request.token)) {
            throw new NotAuthorizedException("Not logged in");
        }
    }
}
