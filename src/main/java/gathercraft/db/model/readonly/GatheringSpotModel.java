package gathercraft.db.model.readonly;

import java.util.Set;

import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

@Entity
@Cacheable
@Cache(usage=CacheConcurrencyStrategy.READ_ONLY)
public class GatheringSpotModel {

    private long id;
    private Set<GatheringPointModel> points;
    private int rarity;

    public GatheringSpotModel() {
        super();
    }

    public GatheringSpotModel(final Set<GatheringPointModel> points, int rarity) {
        super();
        this.points = points;
        this.rarity = rarity;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public long getId() {
        return id;
    }

    public void setId(final long id) {
        this.id = id;
    }

    @Cache(usage=CacheConcurrencyStrategy.READ_ONLY)
    @OneToMany(cascade = CascadeType.ALL)
    public Set<GatheringPointModel> getPoints() {
        return points;
    }

    public void setPoints(final Set<GatheringPointModel> points) {
        this.points = points;
    }

	public int getRarity() {
		return rarity;
	}

	public void setRarity(int rarity) {
		this.rarity = rarity;
	}
}
