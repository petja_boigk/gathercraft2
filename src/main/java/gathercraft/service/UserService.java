package gathercraft.service;

import java.util.Calendar;
import java.util.UUID;

import javax.transaction.Transactional;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.Consumes;
import javax.ws.rs.ForbiddenException;
import javax.ws.rs.InternalServerErrorException;
import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

import gathercraft.db.dao.UserDao.CreateUserResult;
import gathercraft.db.dao.UserDao.UpdateUserResult;
import gathercraft.db.model.UserModel;
import gathercraft.service.request.user.UserRequest;
import gathercraft.service.result.user.UserResult;

@Component
@Path("/login/")
@Transactional
public class UserService extends BaseService {

    @POST
    @Path("/")
    @Consumes(value = MediaType.APPLICATION_JSON_VALUE)
    @Produces(value = MediaType.APPLICATION_JSON_VALUE)
    public UserResult login(final UserRequest request) {
        if (request == null) {
            throw new BadRequestException();
        }
        final UserModel user = userDao.loadUserByLogin(request.login);

        if (user == null || !request.login.equals(user.getLogin())
                || !request.password.equals(user.getPassword())) {
            throw new BadRequestException();
        }

        user.setLastLogin(Calendar.getInstance().getTime());
        user.setToken(UUID.randomUUID().toString());

        final UpdateUserResult result = userDao.updateUser(user);

        if (result == UpdateUserResult.ERROR) {
            throw new InternalServerErrorException();
        }
        if (result == UpdateUserResult.LOGINNOTEXIST) {
            throw new NotAuthorizedException("Login doesnt exist!");
        }

        return new UserResult(user);
    }

    @POST
    @Path("register/")
    @Consumes(value = MediaType.APPLICATION_JSON_VALUE)
    @Produces(value = MediaType.APPLICATION_JSON_VALUE)
    public UserResult register(final UserRequest request) {
        if (request == null) {
            throw new BadRequestException();
        }

        final UserModel user = new UserModel();

        user.setName(request.name);
        user.setPassword(request.password);
        user.setLogin(request.login);
        user.setLastLogin(Calendar.getInstance().getTime());
        user.setToken(UUID.randomUUID().toString());

        if (user.getName().length() < 4 || user.getPassword().length() < 6
                || user.getLogin().length() < 4) {
            throw new BadRequestException();
        }

        final CreateUserResult result = userDao.createUser(user);
        if (result == CreateUserResult.ERROR) {
            throw new InternalServerErrorException();
        }
        if (result == CreateUserResult.LOGINEXIST) {
            throw new ForbiddenException();
        }

        return new UserResult(user);
    }
}
