package gathercraft.db.dao;

import org.springframework.stereotype.Component;

import gathercraft.db.model.readonly.ItemModel;

@Component
public class ItemDao extends BaseDao {

    public ItemModel loadItemById(final long id) {
        return (ItemModel) this.loadById(id, ItemModel.class);
    }
}
